/* eslint-disable no-unused-vars */
"use strict";

/* Copyright (c) 2022-present OpenDevise Inc.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * This file was copied from the Antora Permanent Page Link Extension project. The
 * original file can be found at the following URL:
 * https://gitlab.com/vetalalien/antora-permanent-page-link-extension/lib/index.js
 */

module.exports.register = function () {
  this.once(
    "documentsConverted",
    ({
      playbook,
      siteAsciiDocConfig,
      siteCatalog,
      uiCatalog,
      contentCatalog,
    }) => {
      const pages = contentCatalog.getPages().filter((page) => page.rel);
      pages.forEach((page) => {
        const alias = page.rel;
        page.asciidoc.attributes["page-uuid-url"] = alias.pub.url;
      });
    }
  );
};
