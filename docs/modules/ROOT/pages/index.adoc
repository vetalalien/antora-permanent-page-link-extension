= Notes on using Antora permanent page link extension
:uuid-url: cdd490e-fb91-4fd0-89f7-7a76d580024c5
:page-aliases: {uuid-url}.adoc, abc.adoc

== General

link:{site-url}/{page-component-name}/{uuid-url}[This link] is a permanent link of the current page. It can also be found in the top right corner of the page (in the page header; see the navigation bar item "UUID URL"; let's call it Item).

The value of "href" html attribute placed inside the <a> tag of Item is set  using the current page attribute `page-uuid-url`, the last one was created by link:https://gitlab.com/vetalalien/antora-permanent-page-link-extension[Antora permanent page link extension], the extension was used in the generation of this site.

The next lines (placed in the https://gitlab.com/vetalalien/antora-permanent-page-link-extension/-/blob/main/supplemental-ui/partials/header-content.hbs?ref_type=heads[header-content.hbs] file
footnote:[The file is used for the overriding a page header of Antora default UI. You can read about Antora's supplemental UI files specifying feature https://docs.antora.org/antora/latest/playbook/ui-supplemental-files/[here].]
) show such using of the attribute:
[source, html]
----
include::example$partials/header-content.hbs[tag=uuid-url-using]
----

== Summary

The link can be copied and used anywhere. By agreement within the use of this extension, the authors of the page are obliged to never change the link so that the page can always be found.